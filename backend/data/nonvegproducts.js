const nonvegproducts = [
  {
    _id: 1,
    name: "Women Floral Print Lounge T-Shirt",
    image: "/images/female_img_3.jpg",
    description:
      "Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.",
    brand: "Burberry",
    category: "Women Fashion",
    price: 12000,
    countInStock: 22,
    rating: 4.6,
    numReviews: 4,
  },
  {
    _id: 2,
    name: "Pack of 2 Round Neck T-Shirts",
    image: "/images/male_img_4.jpg",
    description:
      "Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.",
    brand: "Urban Outfitters",
    category: "Men Fashion",
    price: 8500,
    countInStock: 42,
    rating: 4.4,
    numReviews: 2,
  },
  {
    _id: 3,
    name: "Native Youth",
    image: "/images/male_img_2.jpg",
    description:
      "Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.",
    brand: "Ralph Lauren",
    category: "Men Fashion",
    price: 6000,
    countInStock: 18,
    rating: 4.5,
    numReviews: 2,
  },
  {
    _id: 4,
    name: "Print Crop Lounge Top",
    image: "/images/female_img_1.jpg",
    description:
      "Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.",
    brand: "Gucci",
    category: "Women Fashion",
    price: 21000,
    countInStock: 8,
    rating: 4.8,
    numReviews: 12,
  },
];

module.exports = nonvegproducts;
