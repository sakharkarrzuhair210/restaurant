const express = require("express");
const dotenv = require("dotenv");
const nvproducts = require("./data/nonvegproducts");
const vproducts = require("./data/vegproducts");

dotenv.config();

const app = express();

app.get("/", (req, res) => {
  res.send("API is running");
});

app.get("/api/nonvegproducts", (req, res) => {
  res.json(nvproducts);
});

app.get("/api/vegproducts", (req, res) => {
  res.json(vproducts);
});

app.get("/api/nonvegproducts/:id", (req, res) => {
  const nvproduct = nvproducts.find(
    (product) => product._id === +req.params.id
  );
  res.json(nvproduct);
});

app.get("/api/vegproducts/:id", (req, res) => {
  const vproduct = vproducts.find((product) => product._id === +req.params.id);
  res.json(vproduct);
});

const PORT = process.env.PORT || 5006;

app.listen(PORT, () =>
  console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}.`)
);
