import ReactDOM from "react-dom";
import { ChakraProvider } from "@chakra-ui/provider";
import { BrowserRouter } from "react-router-dom";
import App from "./App";

ReactDOM.render(
  <ChakraProvider>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </ChakraProvider>,
  document.querySelector("#root")
);
