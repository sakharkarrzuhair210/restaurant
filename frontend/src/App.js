import { Routes, Route, useRoutes } from "react-router-dom";
import { ChakraProvider } from "@chakra-ui/react";
import { Flex, Image } from "@chakra-ui/react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import MainScreen from "./screens/MainScreen";
import HomeScreen from "./screens/HomeScreen";
import NonVegMenuScreen from "./screens/NonVegMenuScreen";
import VegMenuScreen from "./screens/VegMenuScreen";
import VegProductScreen from "./screens/VegProductScreen";
import NonVegProductScreen from "./screens/NonVegProductScreen";

const App = () => {
  return (
    <ChakraProvider>
      <>
        <Header />
        <Flex
          as="main"
          direction="column"
          mt="72px"
          minH="xl"
          py="8"
          px="8"
          bgColor={"blackAlpha.600"}
        >
          <Routes>
            <Route path="/" element={<MainScreen />} />
            <Route path="/homescreen" element={<HomeScreen />} />
            <Route path="/nonvegmenu" element={<NonVegMenuScreen />} />
            <Route path="/vegmenu" element={<VegMenuScreen />} />
            <Route path="/vegproduct/:id" element={<VegProductScreen />} />
            <Route
              path="/nonvegproduct/:id"
              element={<NonVegProductScreen />}
            />
          </Routes>
        </Flex>

        <Footer />
      </>
    </ChakraProvider>
  );
};

export default App;
