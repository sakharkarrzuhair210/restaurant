const vegproducts = [
  {
    _id: 5,
    name: "Crochet Detail Lightweight Top",
    image: "/images/female_img_2.jpg",
    description:
      "Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.",
    brand: "Chanel",
    category: "Women Fashion",
    price: 43000,
    countInStock: 6,
    rating: 4.9,
    numReviews: 8,
  },
  {
    _id: 6,
    name: "Juventus Henley Neck Jersey",
    image: "/images/male_img_1.jpg",
    description:
      "Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.",
    brand: "Tom Ford",
    category: "Men Fashion",
    price: 16000,
    countInStock: 21,
    rating: 4.1,
    numReviews: 3,
  },
  {
    _id: 7,
    name: "Mavericks Drake Tank Top",
    image: "/images/male_img_3.jpg",
    description:
      "Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.",
    brand: "Calvin Klein",
    category: "Men Fashion",
    price: 7500,
    countInStock: 25,
    rating: 3.4,
    numReviews: 3,
  },
  {
    _id: 8,
    name: "Printed Polo Collar T-shirt",
    image: "/images/female_img_4.jpg",
    description:
      "Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.",
    brand: "Chanel",
    category: "Women Fashion",
    price: 9500,
    countInStock: 0,
    rating: 4.2,
    numReviews: 5,
  },
];

export default vegproducts;
