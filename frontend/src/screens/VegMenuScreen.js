import { useState, useEffect } from "react";
import axios from "axios";
import VegProduct from "../components/VegProduct";
import { Grid, Flex, Button } from "@chakra-ui/react";
import { Link as RouterLink } from "react-router-dom";

const VegMenuScreen = () => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    const fetchProducts = async () => {
      const { data } = await axios.get("/api/vegproducts");
      setProducts(data);
    };

    fetchProducts();
  }, []);

  return (
    <>
      <Flex mb="5">
        <Button as={RouterLink} to="/homescreen" colorScheme="gray">
          Go Back
        </Button>
      </Flex>

      <Grid
        alignItems={"center"}
        justifyContent={"center"}
        templateColumns="repeat(3, 2fr)"
        gap="8"
      >
        {products.map((product) => (
          <VegProduct key={product._id} product={product} />
        ))}
      </Grid>
    </>
  );
};

export default VegMenuScreen;
