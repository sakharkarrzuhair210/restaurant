import { Heading, Grid, Flex, Link, GridItem } from "@chakra-ui/react";

import { Link as RouterLink } from "react-router-dom";

const HomeScreen = () => {
  return (
    <>
      <Heading as="h2" mb="8" fontSize="3xl">
        Latest Products
      </Heading>

      <Grid
        templateColumns="2, 1fr"
        gap={12}
        display={"flex"}
        justifyContent="center"
      >
        <Link as={RouterLink} to="/vegmenu">
          <Flex direction="column">
            <Heading>Veg</Heading>
          </Flex>
        </Link>
        <Link as={RouterLink} to="/nonvegmenu">
          <Flex direction="column">
            <Heading>Non-Veg</Heading>
          </Flex>
        </Link>
      </Grid>
    </>
  );
};

export default HomeScreen;
