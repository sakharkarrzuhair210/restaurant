import { Link as RouterLink } from "react-router-dom";
import { Heading, Flex, Button, Link } from "@chakra-ui/react";

const MainScreen = () => {
  return (
    <>
      <Heading>My Main Screen</Heading>
      <Link as={RouterLink} to="/homescreen">
        <Button>To Home Screen</Button>
      </Link>
    </>
  );
};

export default MainScreen;
